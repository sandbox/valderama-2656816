<?php
/**
 * @file
 * Administrative forms for Mandrill module.
 */


/**
 * Administrative settings.
 *
 * @param array $form
 *   Form render array.
 * @param array $form_state
 *   Array containing form state values.
 *
 * @return array
 *   An array containing form items to place on the module settings page.
 */
function inxmail_admin_settings($form, &$form_state) {

  $form['inxmail_sender_mail'] = array(
    '#title' => t('Inxmail sender mail address'),
    '#type' => 'textfield',
    '#default_value' => variable_get('inxmail_sender_mail'),
  );

  $form['inxmail_server'] = array(
    '#title' => t('Inxmail server'),
    '#type' => 'textfield',
    '#default_value' => variable_get('inxmail_server'),
  );

  $form['inxmail_user'] = array(
    '#title' => t('Inxmail user'),
    '#type' => 'textfield',
    '#default_value' => variable_get('inxmail_user'),
  );

  $form['inxmail_pass'] = array(
    '#title' => t('Inxmail password'),
    '#type' => 'textfield',
    '#default_value' => variable_get('inxmail_pass'),
  );


  return system_settings_form($form);
}


/**
 * Return a form for sending a test email.
 */
function inxmail_test_form($form, &$form_state) {
  global $user;

  $form['inxmail_test_address'] = array(
    '#type' => 'textfield',
    '#title' => t('Email address to send a test email to'),
    '#default_value' => $user->mail,
    '#description' => t('Type in an address to have a test email sent there.'),
    '#required' => TRUE,
  );
  $form['inxmail_test_body'] = array(
    '#type' => 'textarea',
    '#title' => t('Test body contents'),
    '#default_value' => 'This is an inxmail test email.',
  );

  $form['test_submit'] = array(
    '#type' => 'submit',
    '#value' => t('Send test email'),
  );
  $form['test_cancel'] = array(
    '#type' => 'link',
    '#href' => 'admin/config/services/inxmail',
    '#title' => t('Cancel'),
  );

  return $form;
}

/**
 * Submit handler for inxmail_test_form(), sends the test email.
 */
function inxmail_test_form_submit($form, &$form_state) {
  global $language;

  // If an address was given, send a test email message.
  $test_address = $form_state['values']['inxmail_test_address'];

  $params['subject'] = t('Drupal Inxmail test email');
  $params['body'] = $form_state['values']['inxmail_test_body'];

  $mailsystem = mailsystem_get();
  if ($mailsystem['default-system'] != 'InxmailMailSystem') {
    drupal_set_message(t('InxmailMailSystem is not configured as default mail system.'));
  }

  $params['body'] = array('0' => $params['body']);

  $result = drupal_mail('inxmail', 'test', $test_address, $language, $params);

  if (isset($result['result']) && $result['result'] == 'true') {
    drupal_set_message(t('Inxmail test email sent from %from to %to.', array('%from' => $result['from'], '%to' => $result['to'])), 'status');
  }
}
