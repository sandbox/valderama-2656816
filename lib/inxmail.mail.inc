<?php

/**
 * @file
 * Implements Inxmail as a Drupal MailSystemInterface
 */

/**
 * Modify the drupal mail system to use Inxmail when sending emails.
 */
class InxmailMailSystem implements MailSystemInterface {

  /**
   * @var string
   */
  protected $user;

  /**
   * @var string
   */
  protected $pass;

  /**
   * @var string
   */
  protected $server;


  /**
   * @var Inx_Api_Session
   */
  protected $session;

  /**
   * Constructor
   */
  public function __construct() {
    // Register autoload
    Inx_Apiimpl_Loader::registerAutoload();

    $this->server = variable_get('inxmail_server');
    $this->user = variable_get('inxmail_user');
    $this->pass = variable_get('inxmail_pass');
  }

  /**
   * Format a message composed by drupal_mail() prior sending.
   *
   * @param array $message
   *   A message array, as described in hook_mail_alter().
   *
   * @return array
   *   The formatted $message.
   */
  public function format(array $message) {
    // Join the body array into one string.
    $message['body'] = implode("\n\n", $message['body']);
    // Convert any HTML to plain-text.
    $message['body'] = drupal_html_to_text($message['body']);
    // Wrap the mail body for sending.
    $message['body'] = drupal_wrap_mail($message['body']);
    return $message;
  }

  /**
   * Send the email message.
   *
   * @see drupal_mail()
   *
   * @param array $message
   *   A message array, as described in hook_mail_alter().
   *
   * @return bool
   *   TRUE if the mail was successfully accepted, otherwise FALSE.
   */
  public function mail(array $message) {
    // Try to open session.
    try {
      $this->session = Inx_Api_Session::createRemoteSession($this->server, $this->user, $this->pass);
    }
    catch (Inx_Api_LoginException $e) {
      $params = array('!message' => $e->getMessage(), '!code' => $e->getCode());
      drupal_set_message(t('Could not create Inxmail remote session "!message" (Code: !code)', $params), 'error');
      return FALSE;
    }
    catch (Exception $e) {
      drupal_set_message($e->getMessage(), 'error');
      return FALSE;
    }

    // Prepare.
    $recipient = $message['to'];
    $subject = $message['subject'];
    $body = $message['body'];
    $message['headers']; // cc und bcc

    $sender_mail = variable_get('inxmail_sender_mail') != NULL ? variable_get('inxmail_sender_mail') : variable_get('site_mail');

    // Prepare Inxmail stuff.
    $listContextManager = $this->session->getListContextManager();

    $list = $listContextManager->findByName(Inx_Api_List_SystemListContext::NAME);
    $sender = $this->session->getTemporaryMailSender();
    $mailing = $sender->createTemporaryMail($list);

    $mailing->updateRecipientAddress($recipient);
    $mailing->updateSenderAddress($sender_mail);
    $mailing->updateReplyToAddress($sender_mail);
    $mailing->updateSubject($subject);

//    $mailing->setContentHandler('Inx_Api_Mailing_HtmlTextContentHandler');
    $mailing->setContentHandler('Inx_Api_Mailing_PlainTextContentHandler');
    $oContentHandler = $mailing->getContentHandler();
    $oContentHandler->updateContent($body);

    // Send.
    $success = $sender->sendTemporaryMail($mailing);

    // Close session.
    $this->session->close();

    // Pass success upstream.
    return $success;
  }
}
